/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen;

public class FService
{
	// Config Files Paths
	// =======================================================================================================================
	// Standard

    public static final String FILTER_FILE = "config/chatfilter.txt";
    public static final String QUESTION_FILE = "config/questionwords.txt";
    public static final String HEXID_FILE = "config/hexid.properties";
    public static final String SERVER_NAME_FILE = "config/servername.xml";

    public static final String EXTENDER_FILE = "config/extender.properties";
    public static final String TELNET_FILE = "config/telnet.properties";
    public static final String SIEGE_CONFIGURATION_FILE = "config/head/siege.properties";
    public static final String DEVELOPER = "config/functions/developer.properties";
    public static final String CLASS_DAMAGES_FILE = "config/functions/classDamages.properties";
}