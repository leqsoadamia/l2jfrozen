/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver;

import com.l2jfrozen.LoggerManager;
import com.l2jfrozen.ServerTitle;
import com.l2jfrozen.ServerType;
import com.l2jfrozen.configuration.*;
import com.l2jfrozen.crypt.nProtect;
import com.l2jfrozen.database.manager.GameServerManager;
import com.l2jfrozen.database.manager.impl.GameServerManagerImpl;
import com.l2jfrozen.gameserver.ai.special.manager.AILoader;
import com.l2jfrozen.gameserver.cache.CrestCache;
import com.l2jfrozen.gameserver.cache.HtmCache;
import com.l2jfrozen.gameserver.communitybbs.Manager.ForumsBBSManager;
import com.l2jfrozen.gameserver.controllers.GameTimeController;
import com.l2jfrozen.gameserver.controllers.RecipeController;
import com.l2jfrozen.gameserver.controllers.TradeController;
import com.l2jfrozen.gameserver.datatables.*;
import com.l2jfrozen.gameserver.datatables.csv.*;
import com.l2jfrozen.gameserver.datatables.sql.*;
import com.l2jfrozen.gameserver.datatables.xml.AugmentationData;
import com.l2jfrozen.gameserver.datatables.xml.ExperienceData;
import com.l2jfrozen.gameserver.datatables.xml.ZoneData;
import com.l2jfrozen.gameserver.geo.GeoData;
import com.l2jfrozen.gameserver.geo.geoeditorcon.GeoEditorListener;
import com.l2jfrozen.gameserver.geo.pathfinding.PathFinding;
import com.l2jfrozen.gameserver.handler.*;
import com.l2jfrozen.gameserver.idfactory.IdFactory;
import com.l2jfrozen.gameserver.managers.*;
import com.l2jfrozen.gameserver.model.L2Manor;
import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.PartyMatchRoomList;
import com.l2jfrozen.gameserver.model.PartyMatchWaitingList;
import com.l2jfrozen.gameserver.model.entity.Announcements;
import com.l2jfrozen.gameserver.model.entity.Hero;
import com.l2jfrozen.gameserver.model.entity.MonsterRace;
import com.l2jfrozen.gameserver.model.entity.event.manager.EventManager;
import com.l2jfrozen.gameserver.model.entity.olympiad.Olympiad;
import com.l2jfrozen.gameserver.model.entity.sevensigns.SevenSigns;
import com.l2jfrozen.gameserver.model.entity.sevensigns.SevenSignsFestival;
import com.l2jfrozen.gameserver.model.entity.siege.clanhalls.BanditStrongholdSiege;
import com.l2jfrozen.gameserver.model.entity.siege.clanhalls.DevastatedCastle;
import com.l2jfrozen.gameserver.model.entity.siege.clanhalls.FortressOfResistance;
import com.l2jfrozen.gameserver.model.multisell.L2Multisell;
import com.l2jfrozen.gameserver.model.spawn.AutoSpawn;
import com.l2jfrozen.gameserver.network.L2GameClient;
import com.l2jfrozen.gameserver.network.L2GamePacketHandler;
import com.l2jfrozen.gameserver.powerpak.PowerPak;
import com.l2jfrozen.gameserver.script.EventDroplist;
import com.l2jfrozen.gameserver.script.faenor.FaenorScriptEngine;
import com.l2jfrozen.gameserver.scripting.CompiledScriptCache;
import com.l2jfrozen.gameserver.scripting.L2ScriptEngineManager;
import com.l2jfrozen.gameserver.taskmanager.TaskManager;
import com.l2jfrozen.gameserver.thread.LoginServerThread;
import com.l2jfrozen.gameserver.thread.ThreadPoolManager;
import com.l2jfrozen.gameserver.thread.daemons.DeadlockDetector;
import com.l2jfrozen.gameserver.thread.daemons.ItemsAutoDestroy;
import com.l2jfrozen.gameserver.thread.daemons.PcPoint;
import com.l2jfrozen.gameserver.util.DynamicExtension;
import com.l2jfrozen.gameserver.util.sql.SQLQueue;
import com.l2jfrozen.netcore.SelectorConfig;
import com.l2jfrozen.netcore.SelectorThread;
import com.l2jfrozen.util.IPv4Filter;
import com.l2jfrozen.util.Memory;
import com.l2jfrozen.util.Util;
import com.l2jfrozen.util.database.L2DatabaseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Calendar;

public class GameServer {
    //private static Status _statusServer;
    public static final Calendar dateTimeServerStarted = Calendar.getInstance();
    private static final Logger LOGGER = LoggerFactory.getLogger(GameServer.class);
    private static SelectorThread<L2GameClient> _selectorThread;
    private static LoginServerThread LOGGERinThread;
    private static L2GamePacketHandler _gamePacketHandler;
    private static GameServerConfig config = new GameServerConfig();
    private static DatabaseConfig databaseConfig = new DatabaseConfig();

    public static GameServerConfig getConfig() {
        return config;
    }

    public static void main(String[] args) throws Exception {
        ServerType.serverMode = ServerType.MODE_GAMESERVER;
        LoggerManager.getInstance();
        ConfigManager.getInstance().addObserver(config);
        ConfigManager.getInstance().addObserver(FloodProtectorConfig.getInstance());
        ConfigManager.getInstance().addObserver(databaseConfig);
        ConfigManager.getInstance().addObserver(NetworkConfig.getInstance());
        ConfigManager.getInstance().addObserver(ThreadConfig.getInstance());

        ConfigManager.getInstance().nativeReloadConfig();

        long serverLoadStart = System.currentTimeMillis();

        Util.printSection("Team");

        // Print L2jfrozen's Logo
        ServerTitle.info();
        Util.printSection("Database");
        L2DatabaseFactory.getInstance().setDatabaseConfig(databaseConfig);
        L2DatabaseFactory.getInstance().initialize();
        LOGGER.info("L2DatabaseFactory: loaded.");

        Util.printSection("Threads");
        ThreadPoolManager.getInstance();
        if (GameServerConfig.DEADLOCKCHECK_INTIAL_TIME > 0) {
            ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(DeadlockDetector.getInstance(), GameServerConfig.DEADLOCKCHECK_INTIAL_TIME, GameServerConfig.DEADLOCKCHECK_DELAY_TIME);
        }
        new File(GameServerConfig.DATAPACK_ROOT, "data/clans").mkdirs();
        new File(GameServerConfig.DATAPACK_ROOT, "data/crests").mkdirs();
        new File(GameServerConfig.DATAPACK_ROOT, "data/pathnode").mkdirs();
        new File(GameServerConfig.DATAPACK_ROOT, "data/geodata").mkdirs();

        HtmCache.getInstance();
        CrestCache.getInstance();
        L2ScriptEngineManager.getInstance();

        nProtect.getInstance();
        if (nProtect.isEnabled())
            LOGGER.info("nProtect System Enabled");

        Util.printSection("World");
        L2World.getInstance();
        MapRegionTable.getInstance();
        Announcements.getInstance();
        AutoAnnouncementHandler.getInstance();
        if (!IdFactory.getInstance().isInitialized()) {
            LOGGER.info("Could not read object IDs from DB. Please Check Your Data.");
            throw new Exception("Could not initialize the ID connection");
        }
        StaticObjects.getInstance();
        TeleportLocationTable.getInstance();
        PartyMatchWaitingList.getInstance();
        PartyMatchRoomList.getInstance();
        GameTimeController.getInstance();
        CharNameTable.getInstance();
        ExperienceData.getInstance();
        DuelManager.getInstance();

        if (GameServerConfig.ENABLE_CLASS_DAMAGES)
            ClassDamageManager.loadConfig();

        if (GameServerConfig.AUTOSAVE_DELAY_TIME > 0) {
            AutoSaveManager.getInstance().startAutoSaveManager();
        }

        Util.printSection("Skills");
        if (!SkillTable.getInstance().isInitialized()) {
            LOGGER.info("Could not find the extraced files. Please Check Your Data.");
            throw new Exception("Could not initialize the skill table");
        }
        SkillTreeTable.getInstance();
        SkillSpellbookTable.getInstance();
        NobleSkillTable.getInstance();
        HeroSkillTable.getInstance();
        LOGGER.info("Skills: All skills loaded.");

        Util.printSection("Items");
        if (!ItemTable.getInstance().isInitialized()) {
            LOGGER.info("Could not find the extraced files. Please Check Your Data.");
            throw new Exception("Could not initialize the item table");
        }
        ArmorSetsTable.getInstance();
        if (GameServerConfig.CUSTOM_ARMORSETS_TABLE) {
            CustomArmorSetsTable.getInstance();
        }
        ExtractableItemsData.getInstance();
        SummonItemsData.getInstance();
        if (GameServerConfig.ALLOWFISHING)
            FishTable.getInstance();

        Util.printSection("Npc");
        NpcWalkerRoutesTable.getInstance().load();
        if (!NpcTable.getInstance().isInitialized()) {
            LOGGER.info("Could not find the extraced files. Please Check Your Data.");
            throw new Exception("Could not initialize the npc table");
        }

        Util.printSection("Characters");
        if (GameServerConfig.COMMUNITY_TYPE.equals("full")) {
            ForumsBBSManager.getInstance().initRoot();
        }

        ClanTable.getInstance();
        CharTemplateTable.getInstance();
        LevelUpData.getInstance();
        if (!HennaTable.getInstance().isInitialized()) {
            throw new Exception("Could not initialize the Henna Table");
        }

        if (!HennaTreeTable.getInstance().isInitialized()) {
            throw new Exception("Could not initialize the Henna Tree Table");
        }

        if (!HelperBuffTable.getInstance().isInitialized()) {
            throw new Exception("Could not initialize the Helper Buff Table");
        }

        Util.printSection("Geodata");
        GeoData.getInstance();
        if (GameServerConfig.GEODATA == 2) {
            PathFinding.getInstance();
        }

        Util.printSection("Economy");
        TradeController.getInstance();
        L2Multisell.getInstance();
        LOGGER.info("Multisell: loaded.");

        Util.printSection("Clan Halls");
        ClanHallManager.getInstance();
        FortressOfResistance.getInstance();
        DevastatedCastle.getInstance();
        BanditStrongholdSiege.getInstance();
        AuctionManager.getInstance();

        Util.printSection("Zone");
        ZoneData.getInstance();

        Util.printSection("Spawnlist");
        if (!GameServerConfig.ALT_DEV_NO_SPAWNS) {
            SpawnTable.getInstance();
        } else {
            LOGGER.info("Spawn: disable load.");
        }
        if (!GameServerConfig.ALT_DEV_NO_RB) {
            RaidBossSpawnManager.getInstance();
            GrandBossManager.getInstance();
            RaidBossPointsManager.init();
        } else {
            LOGGER.info("RaidBoss: disable load.");
        }
        DayNightSpawnManager.getInstance().notifyChangeMode();

        Util.printSection("Dimensional Rift");
        DimensionalRiftManager.getInstance();

        Util.printSection("Misc");
        RecipeTable.getInstance();
        RecipeController.getInstance();
        EventDroplist.getInstance();
        AugmentationData.getInstance();
        MonsterRace.getInstance();
        MercTicketManager.getInstance();
        PetitionManager.getInstance();
        CursedWeaponsManager.getInstance();
        TaskManager.getInstance();
        L2PetDataTable.getInstance().loadPetsData();
        SQLQueue.getInstance();
        if (GameServerConfig.ACCEPT_GEOEDITOR_CONN) {
            GeoEditorListener.getInstance();
        }
        if (GameServerConfig.SAVE_DROPPED_ITEM) {
            ItemsOnGroundManager.getInstance();
        }
        if (GameServerConfig.AUTODESTROY_ITEM_AFTER > 0 || GameServerConfig.HERB_AUTO_DESTROY_TIME > 0) {
            ItemsAutoDestroy.getInstance();
        }

        Util.printSection("Manor");
        L2Manor.getInstance();
        CastleManorManager.getInstance();

        Util.printSection("Castles");
        CastleManager.getInstance();
        SiegeManager.getInstance();
        FortManager.getInstance();
        FortSiegeManager.getInstance();
        CrownManager.getInstance();

        Util.printSection("Boat");
        BoatManager.getInstance();

        Util.printSection("Doors");
        DoorTable.getInstance().parseData();

        Util.printSection("Four Sepulchers");
        FourSepulchersManager.getInstance();

        Util.printSection("Seven Signs");
        SevenSigns.getInstance();
        SevenSignsFestival.getInstance();
        AutoSpawn.getInstance();
        AutoChatHandler.getInstance();

        Util.printSection("Olympiad System");
        Olympiad.getInstance();
        Hero.getInstance();

        Util.printSection("Access Levels");
        AccessLevels.getInstance();
        AdminCommandAccessRights.getInstance();
        GmListTable.getInstance();

        Util.printSection("Handlers");
        ItemHandler.getInstance();
        SkillHandler.getInstance();
        AdminCommandHandler.getInstance();
        UserCommandHandler.getInstance();
        VoicedCommandHandler.getInstance();

        LOGGER.info("AutoChatHandler : Loaded " + AutoChatHandler.getInstance().size() + " handlers in total.");
        LOGGER.info("AutoSpawnHandler : Loaded " + AutoSpawn.getInstance().size() + " handlers in total.");

        Runtime.getRuntime().addShutdownHook(Shutdown.getInstance());

        try {
            DoorTable doorTable = DoorTable.getInstance();

            // Opened by players like L2OFF
            //doorTable.getDoor(19160010).openMe();
            //doorTable.getDoor(19160011).openMe();

            doorTable.getDoor(19160012).openMe();
            doorTable.getDoor(19160013).openMe();
            doorTable.getDoor(19160014).openMe();
            doorTable.getDoor(19160015).openMe();
            doorTable.getDoor(19160016).openMe();
            doorTable.getDoor(19160017).openMe();
            doorTable.getDoor(24190001).openMe();
            doorTable.getDoor(24190002).openMe();
            doorTable.getDoor(24190003).openMe();
            doorTable.getDoor(24190004).openMe();
            doorTable.getDoor(23180001).openMe();
            doorTable.getDoor(23180002).openMe();
            doorTable.getDoor(23180003).openMe();
            doorTable.getDoor(23180004).openMe();
            doorTable.getDoor(23180005).openMe();
            doorTable.getDoor(23180006).openMe();
            doorTable.checkAutoOpen();
            doorTable = null;
        } catch (NullPointerException e) {
            LOGGER.info("There is errors in your Door.csv file. Update door.csv");
            LOGGER.error("", e);
        }

        Util.printSection("Quests");
        if (!GameServerConfig.ALT_DEV_NO_QUESTS) {
            QuestManager.getInstance();
            QuestManager.getInstance().report();
        } else
            LOGGER.info("Quest: disable load.");

        Util.printSection("AI");
        if (!GameServerConfig.ALT_DEV_NO_AI) {
            AILoader.init();
        } else {
            LOGGER.info("AI: disable load.");
        }

        Util.printSection("Scripts");
        if (!GameServerConfig.ALT_DEV_NO_SCRIPT) {
            File scripts = new File(GameServerConfig.DATAPACK_ROOT, "data/scripts.cfg");
            L2ScriptEngineManager.getInstance().executeScriptsList(scripts);

            CompiledScriptCache compiledScriptCache = L2ScriptEngineManager.getInstance().getCompiledScriptCache();
            if (compiledScriptCache == null)
                LOGGER.info("Compiled Scripts Cache is disabled.");
            else {
                compiledScriptCache.purge();
                if (compiledScriptCache.isModified()) {
                    compiledScriptCache.save();
                    LOGGER.info("Compiled Scripts Cache was saved.");
                } else
                    LOGGER.info("Compiled Scripts Cache is up-to-date.");
            }
            FaenorScriptEngine.getInstance();
        } else {
            LOGGER.info("Script: disable load.");
        }

        Util.printSection("Game Server");

        if (GameServerConfig.IRC_ENABLED)
            IrcManager.getInstance().getConnection().sendChan(GameServerConfig.IRC_MSG_START);

        LOGGER.info("IdFactory: Free ObjectID's remaining: " + IdFactory.getInstance().size());
        try {
            DynamicExtension.getInstance();
        } catch (Exception ex) {
            LOGGER.error("", ex);

            LOGGER.info("DynamicExtension could not be loaded and initialized" + ex);
        }

        Util.printSection("Custom Mods");

        if (GameServerConfig.L2JMOD_ALLOW_WEDDING || GameServerConfig.ALLOW_AWAY_STATUS || GameServerConfig.PCB_ENABLE || GameServerConfig.POWERPAK_ENABLED) {
            if (GameServerConfig.L2JMOD_ALLOW_WEDDING)
                CoupleManager.getInstance();

            if (GameServerConfig.ALLOW_AWAY_STATUS)
                AwayManager.getInstance();

            if (GameServerConfig.PCB_ENABLE)
                ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(PcPoint.getInstance(), GameServerConfig.PCB_INTERVAL * 1000, GameServerConfig.PCB_INTERVAL * 1000);

            if (GameServerConfig.POWERPAK_ENABLED)
                PowerPak.getInstance();
        } else
            LOGGER.info("All custom mods are Disabled.");

        Util.printSection("EventManager");
        EventManager.getInstance().startEventRegistration();

        if (EventManager.TVT_EVENT_ENABLED || EventManager.CTF_EVENT_ENABLED || EventManager.DM_EVENT_ENABLED) {
            if (EventManager.TVT_EVENT_ENABLED)
                LOGGER.info("TVT Event is Enabled.");
            if (EventManager.CTF_EVENT_ENABLED)
                LOGGER.info("CTF Event is Enabled.");
            if (EventManager.DM_EVENT_ENABLED)
                LOGGER.info("DM Event is Enabled.");
        } else
            LOGGER.info("All events are Disabled.");

        if ((GameServerConfig.OFFLINE_TRADE_ENABLE || GameServerConfig.OFFLINE_CRAFT_ENABLE) && GameServerConfig.RESTORE_OFFLINERS)
            OfflineTradeTable.restoreOfflineTraders();

        Util.printSection("Info");
        LOGGER.info("Operating System: " + Util.getOSName() + " " + Util.getOSVersion() + " " + Util.getOSArch());
        LOGGER.info("Available CPUs: " + Util.getAvailableProcessors());
        LOGGER.info("Maximum Numbers of Connected Players: " + GameServerConfig.MAXIMUM_ONLINE_USERS);
        LOGGER.info("GameServer Started, free memory " + Memory.getFreeMemory() + " Mb of " + Memory.getTotalMemory() + " Mb");
        LOGGER.info("Used memory: " + Memory.getUsedMemory() + " MB");

        Util.printSection("Java specific");
        LOGGER.info("JRE name: " + System.getProperty("java.vendor"));
        LOGGER.info("JRE specification version: " + System.getProperty("java.specification.version"));
        LOGGER.info("JRE version: " + System.getProperty("java.version"));
        LOGGER.info("--- Detecting Java Virtual Machine (JVM)");
        LOGGER.info("JVM installation directory: " + System.getProperty("java.home"));
        LOGGER.info("JVM Avaible Memory(RAM): " + Runtime.getRuntime().maxMemory() / 1048576 + " MB");
        LOGGER.info("JVM specification version: " + System.getProperty("java.vm.specification.version"));
        LOGGER.info("JVM specification vendor: " + System.getProperty("java.vm.specification.vendor"));
        LOGGER.info("JVM specification name: " + System.getProperty("java.vm.specification.name"));
        LOGGER.info("JVM implementation version: " + System.getProperty("java.vm.version"));
        LOGGER.info("JVM implementation vendor: " + System.getProperty("java.vm.vendor"));
        LOGGER.info("JVM implementation name: " + System.getProperty("java.vm.name"));

        Util.printSection("Status");
        System.gc();
        LOGGER.info("Server Loaded in " + (System.currentTimeMillis() - serverLoadStart) / 1000 + " seconds");
        ServerStatus.getInstance();

        Util.printSection("database.login");
        LOGGERinThread = LoginServerThread.getInstance();
        LOGGERinThread.start();

        final SelectorConfig sc = new SelectorConfig();
        sc.MAX_READ_PER_PASS = NetworkConfig.getInstance().MMO_MAX_READ_PER_PASS;
        sc.MAX_SEND_PER_PASS = NetworkConfig.getInstance().MMO_MAX_SEND_PER_PASS;
        sc.SLEEP_TIME = NetworkConfig.getInstance().MMO_SELECTOR_SLEEP_TIME;
        sc.HELPER_BUFFER_COUNT = NetworkConfig.getInstance().MMO_HELPER_BUFFER_COUNT;

        _gamePacketHandler = new L2GamePacketHandler();

        _selectorThread = new SelectorThread<L2GameClient>(sc, _gamePacketHandler, _gamePacketHandler, _gamePacketHandler, new IPv4Filter());

        InetAddress bindAddress = null;
        if (!GameServerConfig.GAMESERVER_HOSTNAME.equals("*")) {
            try {
                bindAddress = InetAddress.getByName(GameServerConfig.GAMESERVER_HOSTNAME);
            } catch (UnknownHostException e1) {
                LOGGER.error("", e1);

                LOGGER.warn("WARNING: The GameServer bind address is invalid, using all avaliable IPs. Reason: " + e1.getMessage(), e1);
            }
        }

        try {
            _selectorThread.openServerSocket(bindAddress, GameServerConfig.PORT_GAME);
        } catch (IOException e) {
            LOGGER.error("", e);

            LOGGER.error("FATAL: Failed to open server socket. Reason: " + e.getMessage(), e);
            System.exit(1);
        }
        _selectorThread.start();
    }

    public static SelectorThread<L2GameClient> getSelectorThread() {
        return _selectorThread;
    }
}