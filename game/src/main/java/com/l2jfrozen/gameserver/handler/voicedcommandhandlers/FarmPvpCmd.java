/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package com.l2jfrozen.gameserver.handler.voicedcommandhandlers;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.gameserver.handler.IVoicedCommandHandler;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.serverpackets.SetupGauge;
import com.l2jfrozen.gameserver.thread.ThreadPoolManager;

public class FarmPvpCmd implements IVoicedCommandHandler {
    private static final String[] VOICED_COMMANDS =
            {
                    "farm1", "farm2", "pvp1", "pvp2"
            };

    @Override
    public boolean useVoicedCommand(String command, L2PcInstance activeChar, String target) {
        int placex;
        int placey;
        int placez;
        String message;

        if (command.equalsIgnoreCase("farm1") && GameServerConfig.ALLOW_FARM1_COMMAND) {
            placex = GameServerConfig.FARM1_X;
            placey = GameServerConfig.FARM1_Y;
            placez = GameServerConfig.FARM1_Z;
            message = GameServerConfig.FARM1_CUSTOM_MESSAGE;
        } else if (command.equalsIgnoreCase("farm2") && GameServerConfig.ALLOW_FARM2_COMMAND) {
            placex = GameServerConfig.FARM2_X;
            placey = GameServerConfig.FARM2_Y;
            placez = GameServerConfig.FARM2_Z;
            message = GameServerConfig.FARM2_CUSTOM_MESSAGE;
        } else if (command.equalsIgnoreCase("pvp1") && GameServerConfig.ALLOW_PVP1_COMMAND) {
            placex = GameServerConfig.PVP1_X;
            placey = GameServerConfig.PVP1_Y;
            placez = GameServerConfig.PVP1_Z;
            message = GameServerConfig.PVP1_CUSTOM_MESSAGE;
        } else if (command.equalsIgnoreCase("pvp2") && GameServerConfig.ALLOW_PVP2_COMMAND) {
            placex = GameServerConfig.PVP2_X;
            placey = GameServerConfig.PVP2_Y;
            placez = GameServerConfig.PVP2_Z;
            message = GameServerConfig.PVP2_CUSTOM_MESSAGE;
        } else {
            return false;
        }

        if (activeChar.isInJail()) {
            activeChar.sendMessage("Sorry,you are in Jail!");
            return false;
        } else if (activeChar.isInOlympiadMode()) {
            activeChar.sendMessage("Sorry,you are in the Olympiad now.");
            return false;
        } else if (activeChar.isInFunEvent()) {
            activeChar.sendMessage("Sorry,you are in an event.");
            return false;
        } else if (activeChar.isInDuel()) {
            activeChar.sendMessage("Sorry,you are in a duel!");
            return false;
        } else if (activeChar.inObserverMode()) {
            activeChar.sendMessage("Sorry,you are in the observation mode.");
            return false;
        } else if (activeChar.isFestivalParticipant()) {
            activeChar.sendMessage("Sorry,you are in a festival.");
            return false;
        } else if (!GameServerConfig.ALT_GAME_KARMA_PLAYER_CAN_USE_GK && activeChar.getKarma() > 0) {
            activeChar.sendMessage("Sorry,you are PK");
            return false;
        }

        SetupGauge sg = new SetupGauge(SetupGauge.BLUE, 15000);
        activeChar.sendPacket(sg);
        sg = null;
        activeChar.setIsImobilised(true);

        ThreadPoolManager.getInstance().scheduleGeneral(new teleportTask(activeChar, placex, placey, placez, message), 15000);

        return true;
    }

    @Override
    public String[] getVoicedCommandList() {
        return VOICED_COMMANDS;
    }

    class teleportTask implements Runnable {
        private final L2PcInstance _activeChar;
        private final int _x;
        private final int _y;
        private final int _z;
        private final String _message;

        teleportTask(L2PcInstance activeChar, int x, int y, int z, String message) {
            _activeChar = activeChar;
            _x = x;
            _y = y;
            _z = z;
            _message = message;
        }

        @Override
        public void run() {
            if (_activeChar == null) {
                return;
            }

            _activeChar.teleToLocation(_x, _y, _z);
            _activeChar.sendMessage(_message);
            _activeChar.setIsImobilised(false);
        }
    }
}
