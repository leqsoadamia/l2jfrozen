/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.gameserver.managers;

import com.l2jfrozen.util.CloseUtil;
import com.l2jfrozen.util.database.L2DatabaseFactory;
import javolution.util.FastMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * control for Custom Npcs that look like players.
 *
 * @author Darki699
 * @version 1.00
 */
public final class CustomNpcInstanceManager {
    private final static Logger LOGGER = LoggerFactory.getLogger(CustomNpcInstanceManager.class.getName());
    private static CustomNpcInstanceManager _instance;
    private FastMap<Integer, CustomNpc> spawns; // <Object id , info>
    private FastMap<Integer, CustomNpc> templates; // <Npc Template Id , info>

    public final class CustomNpc {
        private int spawn = 0;
        private int templateId = 0;
        private int race = 0;
        private int classId = 0;
        private int hairStyle = 0;
        private int hairColor = 0;
        private int face = 0;
        private int nameColor = 0;
        private int titleColor = 0;
        private int karma = 0;
        private int wpnEnchant = 0;
        private int rightHand = 0;
        private int leftHand = 0;
        private int gloves = 0;
        private int chest = 0;
        private int legs = 0;
        private int feet = 0;
        private int hair = 0;
        private int hair2 = 0;
        private int pledge = 0;
        private int cwLevel = 0;
        private int clanId = 0;
        private int allyId = 0;
        private int clanCrest = 0;
        private int allyCrest = 0;
        private int maxRndEnchant = 0;

        private boolean female = false;
        private boolean noble = false;
        private boolean hero = false;
        private boolean pvp = false;
        private boolean rndClass = false;
        private boolean rndAppearance = false;
        private boolean rndWeapon = false;
        private boolean rndArmor = false;

        private String name = "";
        private String title = "";

        public int getRace() {
            return race;
        }

        public void setRace(int race) {
            this.race = race;
        }

        public int getSpawn() {
            return spawn;
        }

        public void setSpawn(int spawn) {
            this.spawn = spawn;
        }

        public int getTemplateId() {
            return templateId;
        }

        public void setTemplateId(int templateId) {
            this.templateId = templateId;
        }

        public int getClassId() {
            return classId;
        }

        public void setClassId(int classId) {
            this.classId = classId;
        }

        public int getHairStyle() {
            return hairStyle;
        }

        public void setHairStyle(int hairStyle) {
            this.hairStyle = hairStyle;
        }

        public int getHairColor() {
            return hairColor;
        }

        public void setHairColor(int hairColor) {
            this.hairColor = hairColor;
        }

        public int getFace() {
            return face;
        }

        public void setFace(int face) {
            this.face = face;
        }

        public int getNameColor() {
            return nameColor;
        }

        public void setNameColor(int nameColor) {
            this.nameColor = nameColor;
        }

        public int getTitleColor() {
            return titleColor;
        }

        public void setTitleColor(int titleColor) {
            this.titleColor = titleColor;
        }

        public int getKarma() {
            return karma;
        }

        public void setKarma(int karma) {
            this.karma = karma;
        }

        public int getWpnEnchant() {
            return wpnEnchant;
        }

        public void setWpnEnchant(int wpnEnchant) {
            this.wpnEnchant = wpnEnchant;
        }

        public int getRightHand() {
            return rightHand;
        }

        public void setRightHand(int rightHand) {
            this.rightHand = rightHand;
        }

        public int getLeftHand() {
            return leftHand;
        }

        public void setLeftHand(int leftHand) {
            this.leftHand = leftHand;
        }

        public int getGloves() {
            return gloves;
        }

        public void setGloves(int gloves) {
            this.gloves = gloves;
        }

        public int getChest() {
            return chest;
        }

        public void setChest(int chest) {
            this.chest = chest;
        }

        public int getLegs() {
            return legs;
        }

        public void setLegs(int legs) {
            this.legs = legs;
        }

        public int getFeet() {
            return feet;
        }

        public void setFeet(int feet) {
            this.feet = feet;
        }

        public int getHair() {
            return hair;
        }

        public void setHair(int hair) {
            this.hair = hair;
        }

        public int getHair2() {
            return hair2;
        }

        public void setHair2(int hair2) {
            this.hair2 = hair2;
        }

        public int getPledge() {
            return pledge;
        }

        public void setPledge(int pledge) {
            this.pledge = pledge;
        }

        public int getCwLevel() {
            return cwLevel;
        }

        public void setCwLevel(int cwLevel) {
            this.cwLevel = cwLevel;
        }

        public int getClanId() {
            return clanId;
        }

        public void setClanId(int clanId) {
            this.clanId = clanId;
        }

        public int getAllyId() {
            return allyId;
        }

        public void setAllyId(int allyId) {
            this.allyId = allyId;
        }

        public int getClanCrest() {
            return clanCrest;
        }

        public void setClanCrest(int clanCrest) {
            this.clanCrest = clanCrest;
        }

        public int getAllyCrest() {
            return allyCrest;
        }

        public void setAllyCrest(int allyCrest) {
            this.allyCrest = allyCrest;
        }

        public int getMaxRndEnchant() {
            return maxRndEnchant;
        }

        public void setMaxRndEnchant(int maxRndEnchant) {
            this.maxRndEnchant = maxRndEnchant;
        }

        public boolean isFemale() {
            return female;
        }

        public void setFemale(boolean female) {
            this.female = female;
        }

        public boolean isNoble() {
            return noble;
        }

        public void setNoble(boolean noble) {
            this.noble = noble;
        }

        public boolean isHero() {
            return hero;
        }

        public void setHero(boolean hero) {
            this.hero = hero;
        }

        public boolean isPvp() {
            return pvp;
        }

        public void setPvp(boolean pvp) {
            this.pvp = pvp;
        }

        public boolean isRndClass() {
            return rndClass;
        }

        public void setRndClass(boolean rndClass) {
            this.rndClass = rndClass;
        }

        public boolean isRndAppearance() {
            return rndAppearance;
        }

        public void setRndAppearance(boolean rndAppearance) {
            this.rndAppearance = rndAppearance;
        }

        public boolean isRndWeapon() {
            return rndWeapon;
        }

        public void setRndWeapon(boolean rndWeapon) {
            this.rndWeapon = rndWeapon;
        }

        public boolean isRndArmor() {
            return rndArmor;
        }

        public void setRndArmor(boolean rndArmor) {
            this.rndArmor = rndArmor;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    /**
     * Constructor Calls to load the data
     */
    CustomNpcInstanceManager() {
        load();
    }

    /**
     * Initiates the manager (if not initiated yet) and/or returns <b>this</b> manager instance
     *
     * @return CustomNpcInstanceManager _instance
     */
    public final static CustomNpcInstanceManager getInstance() {
        if (_instance == null) {
            _instance = new CustomNpcInstanceManager();
        }
        return _instance;
    }

    /**
     * Flush the old data, and load new data
     */
    public final void reload() {
        if (spawns != null) {
            spawns.clear();
        }
        if (templates != null) {
            templates.clear();
        }
        spawns = null;
        templates = null;

        load();
    }

    /**
     * Just load the data for mysql...
     */
    private final void load() {
        if (spawns == null || templates == null) {
            spawns = new FastMap<>();
            templates = new FastMap<>();
        }

        String[] SQL_ITEM_SELECTS =
                {
                        "SELECT" + " spawn,template,name,title,class_id,female,hair_style,hair_color,face,name_color,title_color," + " noble,hero,pvp,karma,wpn_enchant,right_hand,left_hand,gloves,chest,legs,feet,hair,hair2," + " pledge,cw_level,clan_id,ally_id,clan_crest,ally_crest,rnd_class,rnd_appearance,rnd_weapon,rnd_armor,max_rnd_enchant" + " FROM npc_to_pc_polymorph",
                };

        Connection con = null;
        try {
            int count = 0;
            con = L2DatabaseFactory.getInstance().getConnection(false);
            for (String selectQuery : SQL_ITEM_SELECTS) {
                PreparedStatement statement = con.prepareStatement(selectQuery);
                ResultSet rset = statement.executeQuery();

                while (rset.next()) {
                    count++;
                    CustomNpc customNpc = new CustomNpc();
                    customNpc.setSpawn(rset.getInt("spawn"));
                    customNpc.setTemplateId(rset.getInt("template"));
                    customNpc.setName(rset.getString("name"));
                    customNpc.setTitle(rset.getString("title"));
                    customNpc.setClanId(rset.getInt("class_id"));
                    customNpc.setFemale(rset.getInt("female") > 0);
                    customNpc.setHairStyle(rset.getInt("hair_style"));
                    customNpc.setHairColor(rset.getInt("hair_color"));
                    customNpc.setFace(rset.getInt("face"));
                    customNpc.setNameColor(rset.getInt("name_color"));
                    customNpc.setTitleColor(rset.getInt("title_color"));
                    customNpc.setNoble(rset.getInt("noble") > 0);
                    customNpc.setHero(rset.getInt("hero") > 0);
                    customNpc.setPvp(rset.getInt("pvp") > 0);
                    customNpc.setKarma(rset.getInt("karma"));
                    customNpc.setWpnEnchant(rset.getInt("wpn_enchant"));
                    customNpc.setRightHand(rset.getInt("right_hand"));
                    customNpc.setLeftHand(rset.getInt("left_hand"));
                    customNpc.setGloves(rset.getInt("gloves"));
                    customNpc.setChest(rset.getInt("chest"));
                    customNpc.setLegs(rset.getInt("legs"));
                    customNpc.setFeet(rset.getInt("feet"));
                    customNpc.setHair(rset.getInt("hair"));
                    customNpc.setHair2(rset.getInt("hair2"));
                    customNpc.setPledge(rset.getInt("pledge"));
                    customNpc.setCwLevel(rset.getInt("cw_level"));
                    customNpc.setClanId(rset.getInt("clan_id"));
                    customNpc.setAllyId(rset.getInt("ally_id"));
                    customNpc.setClanCrest(rset.getInt("clan_crest"));
                    customNpc.setAllyCrest(rset.getInt("ally_crest"));
                    customNpc.setRndClass(rset.getInt("rnd_class") > 0);
                    customNpc.setRndAppearance(rset.getInt("rnd_appearance") > 0);
                    customNpc.setRndWeapon(rset.getInt("rnd_weapon") > 0);
                    customNpc.setRndArmor(rset.getInt("rnd_armor") > 0);
                    customNpc.setMaxRndEnchant(rset.getInt("max_rnd_enchant"));

                    try {
                        // Same object goes in twice:
                        if (customNpc.getTemplateId() != 0 && !templates.containsKey(customNpc.getTemplateId())) {
                            templates.put(customNpc.getTemplateId(), customNpc);
                        }
                        if (customNpc.getTemplateId() == 0 && !spawns.containsKey(customNpc.getSpawn())) {
                            spawns.put(customNpc.getSpawn(), customNpc);
                        }
                    } catch (Throwable t) {
                        LOGGER.error("", t);

                        LOGGER.warn("Failed to load Npc Morph data for Object Id: " + customNpc.getSpawn() + " template: " + customNpc.getTemplateId());
                    }
                }
                statement.close();
                statement = null;
                rset.close();
                rset = null;
            }
            LOGGER.info("CustomNpcInstanceManager: loaded " + count + " NPC to PC polymorphs.");
        } catch (Exception e) {
            LOGGER.error("", e);
        } finally {
            CloseUtil.close(con);
            con = null;
        }
    }

    /**
     * Checks if the L2NpcInstance calling this function has polymorphing data
     *
     * @param spawnId - L2NpcInstance's unique Object id
     * @param npcId   - L2NpcInstance's npc template id
     * @return
     */
    public final boolean isThisL2CustomNpcInstance(int spawnId, int npcId) {
        if (spawnId == 0 || npcId == 0) {
            return false;
        } else if (spawns.containsKey(spawnId)) {
            return true;
        } else {
            return templates.containsKey(npcId);
        }
    }

    /**
     * Return the polymorphing data for this L2NpcInstance if the data exists
     *
     * @param spawnId - NpcInstance's unique Object Id
     * @param npcId   - NpcInstance's npc template Id
     * @return CustomInfo type data pack, or null if no such data exists.
     */
    public final CustomNpc getCustomData(int spawnId, int npcId) {
        if (spawnId == 0 || npcId == 0) {
            return null;
        }

        //First check individual spawn objects - incase they have different values than their template
        for (CustomNpc ci : spawns.values()) {
            if (ci != null && ci.getSpawn() == spawnId) {
                return ci;
            }
        }

        //Now check if templates contains the morph npc template
        for (CustomNpc ci : templates.values()) {
            if (ci != null && ci.getTemplateId() == npcId) {
                return ci;
            }
        }

        return null;
    }

    /**
     * @return all template morphing queue
     */
    public final FastMap<Integer, CustomNpc> getAllTemplates() {
        return templates;
    }

    /**
     * @return all spawns morphing queue
     */
    public final FastMap<Integer, CustomNpc> getAllSpawns() {
        return spawns;
    }

    /**
     * Already removed CustomInfo - Change is saved in the DB <b>NOT IMPLEMENTED YET!</b>
     *
     * @param ciToRemove
     */
    public final void updateRemoveInDB(CustomNpc ciToRemove) {
        //
    }

    public final void AddInDB(CustomNpc ciToAdd) {
        String Query = "REPLACE INTO npc_to_pc_polymorph VALUES" + " spawn,template,name,title,class_id,female,hair_style,hair_color,face,name_color,title_color," + " noble,hero,pvp,karma,wpn_enchant,right_hand,left_hand,gloves,chest,legs,feet,hair,hair2," + " pledge,cw_level,clan_id,ally_id,clan_crest,ally_crest,rnd_class,rnd_appearance,rnd_weapon,rnd_armor,max_rnd_enchant" + " FROM npc_to_pc_polymorph";

        Connection con = null;

        try {
            con = L2DatabaseFactory.getInstance().getConnection(false);
            PreparedStatement statement = con.prepareStatement(Query);
            ResultSet rset = statement.executeQuery();

            statement.close();
            statement = null;

            while (rset.next()) {
                CustomNpc ci = new CustomNpc();
                ci.setSpawn(rset.getInt("spawn"));
                ci.setTemplateId(rset.getInt("template"));
            }

            rset.close();
            rset = null;
            Query = null;
        } catch (Throwable t) {
            LOGGER.error("", t);

            LOGGER.warn("Could not add Npc Morph info into the DB: ");
        } finally {
            CloseUtil.close(con);
            con = null;
        }
    }
}
