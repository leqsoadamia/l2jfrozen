package com.l2jfrozen.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: vdidenko
 * Date: 11/21/13
 * Time: 10:58 PM
 */
public class LoginServerConfig implements ConfigManagerObserver {
    public static boolean ENABLE_ALL_EXCEPTIONS = true;
    public static String GAME_SERVER_LOGIN_HOST;
    public static String INTERNAL_HOSTNAME;
    public static String EXTERNAL_HOSTNAME;
    public static int GAME_SERVER_LOGIN_PORT;
    public static String LOGIN_BIND_ADDRESS;
    public static int LOGIN_TRY_BEFORE_BAN;
    public static int LOGIN_BLOCK_AFTER_BAN;
    public static int PORT_LOGIN;
    public static boolean ACCEPT_NEW_GAMESERVER;
    public static int DATABASE_TIMEOUT;
    public static int DATABASE_CONNECTION_TIMEOUT;
    public static int DATABASE_PARTITION_COUNT;
    public static boolean ENABLE_DDOS_PROTECTION_SYSTEM;
    public static boolean ENABLE_DEBUG_DDOS_PROTECTION_SYSTEM;
    public static String DDOS_COMMAND_BLOCK;
    public static int BRUT_AVG_TIME;
    public static int BRUT_LOGON_ATTEMPTS;
    public static int BRUT_BAN_IP_TIME;
    public static boolean SHOW_LICENCE;
    public static boolean FORCE_GGAUTH;
    public static boolean FLOOD_PROTECTION;
    public static int FAST_CONNECTION_LIMIT;
    public static int NORMAL_CONNECTION_TIME;
    public static int FAST_CONNECTION_TIME;
    public static int MAX_CONNECTION_PER_IP;
    public static boolean AUTO_CREATE_ACCOUNTS;
    public static String NETWORK_IP_LIST;
    public static long SESSION_TTL;
    public static int MAX_LOGINSESSIONS;
    public static int IP_UPDATE_TIME;
    public static boolean DEBUG;
    public static boolean DEVELOPER;
    public static boolean DEBUG_PACKETS;
    public static boolean IS_TELNET_ENABLED;
    private static ConfigManager configManager = ConfigManager.getInstance();

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginServerConfig.class);

    public LoginServerConfig() {
        LOGGER.info("Initialize login config");
    }

    @Override
    public void configurationLoad() {
        GAME_SERVER_LOGIN_HOST = configManager.getString("LoginHostname");
        GAME_SERVER_LOGIN_PORT = configManager.getInteger("LoginPort");

        LOGIN_BIND_ADDRESS = configManager.getString("LoginserverHostname");
        PORT_LOGIN = configManager.getInteger("LoginserverPort");

        ACCEPT_NEW_GAMESERVER = configManager.getBoolean("AcceptNewGameServer");

        LOGIN_TRY_BEFORE_BAN = configManager.getInteger("LoginTryBeforeBan");
        LOGIN_BLOCK_AFTER_BAN = configManager.getInteger("LoginBlockAfterBan");

        INTERNAL_HOSTNAME = configManager.getString("InternalHostname");
        EXTERNAL_HOSTNAME = configManager.getString("ExternalHostname");


        ENABLE_DDOS_PROTECTION_SYSTEM = configManager.getBoolean("EnableDdosProSystem");
        DDOS_COMMAND_BLOCK = configManager.getString("Deny_noallow_ip_ddos");
        ENABLE_DEBUG_DDOS_PROTECTION_SYSTEM = configManager.getBoolean("Fulllog_mode_print");

        DATABASE_TIMEOUT = configManager.getInteger("LoginTimeOutConDb");
        DATABASE_CONNECTION_TIMEOUT = configManager.getInteger("SingleConnectionTimeOutDb");
        DATABASE_PARTITION_COUNT = configManager.getInteger("LoginPartitionCount");

        // Anti Brute force attack on login
        BRUT_AVG_TIME = configManager.getInteger("BrutAvgTime"); // in Seconds
        BRUT_LOGON_ATTEMPTS = configManager.getInteger("BrutLogonAttempts");
        BRUT_BAN_IP_TIME = configManager.getInteger("BrutBanIpTime"); // in Seconds

        SHOW_LICENCE = configManager.getBoolean("ShowLicence");
        IP_UPDATE_TIME = configManager.getInteger("IpUpdateTime");
        FORCE_GGAUTH = configManager.getBoolean("ForceGGAuth");

        AUTO_CREATE_ACCOUNTS = configManager.getBoolean("AutoCreateAccounts");

        FLOOD_PROTECTION = configManager.getBoolean("EnableFloodProtection");
        FAST_CONNECTION_LIMIT = configManager.getInteger("FastConnectionLimit");
        NORMAL_CONNECTION_TIME = configManager.getInteger("NormalConnectionTime");
        FAST_CONNECTION_TIME = configManager.getInteger("FastConnectionTime");
        MAX_CONNECTION_PER_IP = configManager.getInteger("MaxConnectionPerIP");
        DEBUG = configManager.getBoolean("Debug");
        DEVELOPER = configManager.getBoolean("LoginDeveloper");

        NETWORK_IP_LIST = configManager.getString("NetworkList");
        SESSION_TTL = configManager.getLong("SessionTTL");
        MAX_LOGINSESSIONS = configManager.getInteger("MaxSessions");

        DEBUG_PACKETS = configManager.getBoolean("LoginDebugPackets");
        IS_TELNET_ENABLED = configManager.getBoolean("EnableTelnet");
        ENABLE_ALL_EXCEPTIONS = configManager.getBoolean("EnableAllExceptionsLog");

    }
}
