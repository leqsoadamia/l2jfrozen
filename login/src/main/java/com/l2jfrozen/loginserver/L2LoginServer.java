/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.loginserver;

import com.l2jfrozen.LoggerManager;
import com.l2jfrozen.ServerTitle;
import com.l2jfrozen.ServerType;
import com.l2jfrozen.configuration.ConfigManager;
import com.l2jfrozen.configuration.DatabaseConfig;
import com.l2jfrozen.configuration.LoginServerConfig;
import com.l2jfrozen.configuration.NetworkConfig;
import com.l2jfrozen.netcore.SelectorConfig;
import com.l2jfrozen.netcore.SelectorThread;
import com.l2jfrozen.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;

public class L2LoginServer {
    public static final int PROTOCOL_REV = 0x0102;
    private static L2LoginServer _instance;
    private static LoginServerConfig config;
    private static DatabaseConfig databaseConfig;
    private static final Logger LOGGER = LoggerFactory.getLogger(L2LoginServer.class.getName());
    private GameServerListener _gameServerListener;
    private SelectorThread<L2LoginClient> _selectorThread;

    public L2LoginServer() {
        ServerType.serverMode = ServerType.MODE_LOGINSERVER;


        // Team info
        Util.printSection("");
        ServerTitle.info();

        try {
            LoginController.load();
        } catch (GeneralSecurityException e) {
            LOGGER.error("FATAL: Failed initializing LoginController. Reason: ",e);
            System.exit(1);
        }

        InetAddress bindAddress = null;
        if (!LoginServerConfig.LOGIN_BIND_ADDRESS.equals("*")) {
            try {
                bindAddress = InetAddress.getByName(LoginServerConfig.LOGIN_BIND_ADDRESS);
            } catch (UnknownHostException e1) {
                LOGGER.error("WARNING: The LoginServer bind address is invalid, using all avaliable IPs. Reason: ",e1);
            }
        }

        final SelectorConfig sc = new SelectorConfig();
        sc.MAX_READ_PER_PASS = NetworkConfig.getInstance().MMO_MAX_READ_PER_PASS;
        sc.MAX_SEND_PER_PASS = NetworkConfig.getInstance().MMO_MAX_SEND_PER_PASS;
        sc.SLEEP_TIME = NetworkConfig.getInstance().MMO_SELECTOR_SLEEP_TIME;
        sc.HELPER_BUFFER_COUNT = NetworkConfig.getInstance().MMO_HELPER_BUFFER_COUNT;

        final L2LoginPacketHandler lph = new L2LoginPacketHandler();
        final SelectorHelper sh = new SelectorHelper();
        try {
            _selectorThread = new SelectorThread<>(sc, sh, lph, sh, sh);
        } catch (IOException e) {
            LOGGER.error("FATAL: Failed to open Selector. Reason: ", e);
            System.exit(1);
        }

        try {
            _gameServerListener = new GameServerListener();
            _gameServerListener.start();
            LOGGER.info("Listening for GameServers on " + LoginServerConfig.GAME_SERVER_LOGIN_HOST + ":" + LoginServerConfig.GAME_SERVER_LOGIN_PORT);
        } catch (IOException e) {
            LOGGER.error("FATAL: Failed to start the Game Server Listener. Reason: ", e);

            System.exit(1);
        }

        try {
            _selectorThread.openServerSocket(bindAddress, LoginServerConfig.PORT_LOGIN);
            _selectorThread.start();
            LOGGER.info("Login Server ready on " + (bindAddress == null ? "*" : bindAddress.getHostAddress()) + ":" + LoginServerConfig.PORT_LOGIN);

        } catch (IOException e) {
            LOGGER.error("FATAL: Failed to open server socket. Reason: ",e);
            System.exit(1);
        }
    }

    public static void main(String[] args) throws SQLException {
        LoggerManager.getInstance();
        config = new LoginServerConfig();
        databaseConfig = new DatabaseConfig();
        ConfigManager.getInstance().addObserver(config);
        ConfigManager.getInstance().addObserver(databaseConfig);

        ConfigManager.getInstance().addObserver(NetworkConfig.getInstance());
        ConfigManager.getInstance().nativeReloadConfig();
        _instance = new L2LoginServer();
    }

    public static L2LoginServer getInstance() {
        return _instance;
    }

    public GameServerListener getGameServerListener() {
        return _gameServerListener;
    }

    public void shutdown(boolean restart) {
        LoginController.getInstance().shutdown();
        System.gc();
        Runtime.getRuntime().exit(restart ? 2 : 0);
    }
}
