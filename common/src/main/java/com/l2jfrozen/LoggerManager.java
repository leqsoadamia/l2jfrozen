package com.l2jfrozen;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

/**
 * User: vdidenko
 * Date: 11/20/13
 * Time: 3:32 AM
 */
public class LoggerManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerManager.class);
    public static final String LOGGER_FILE = "./config/logger.properties";
    public static final String LOG4J_FILE = "./config/log4j.properties";
    public static LoggerManager LOGGER_MANAGER=null;


    public static LoggerManager getInstance() {
        if(LOGGER_MANAGER==null){
            LOGGER_MANAGER=new LoggerManager();
        }
        return LOGGER_MANAGER;
    }
    private LoggerManager() {
        try {
            Properties properties = new Properties();
            InputStream inputStream = new FileInputStream(new File(LOGGER_FILE));
            properties.load(inputStream);
            inputStream.close();
            Boolean loggerAccept = Boolean.parseBoolean(properties.getProperty("papertrail.logs"));
            Properties papertrailProperties = new Properties();
            inputStream = new FileInputStream(new File(LOG4J_FILE));
            papertrailProperties.load(inputStream);

            if (loggerAccept) {
                papertrailProperties.setProperty("log4j.rootLogger",papertrailProperties.getProperty("log4j.rootLogger")+","+"papertrail");
                for(Map.Entry<Object,Object> entry:properties.entrySet()){
                    String key=entry.getKey().toString();
                    String value=entry.getValue().toString();
                    papertrailProperties.setProperty(key,value);
                }
            }
            PropertyConfigurator.configure(papertrailProperties);
        } catch (IOException e) {
            LOGGER.error("Logger properties file not found " + e.getMessage());
        }
    }
}
