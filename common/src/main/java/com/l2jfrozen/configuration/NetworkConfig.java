package com.l2jfrozen.configuration;

import javolution.util.FastList;

import java.util.StringTokenizer;

public class NetworkConfig implements ConfigManagerObserver {
    private static ConfigManager configManager = ConfigManager.getInstance();
    //============================================================
    private static NetworkConfig networkConfig;
    public boolean PACKET_HANDLER_DEBUG;
    /**
     * MMO settings
     */
    public int MMO_SELECTOR_SLEEP_TIME = 20;        // default 20
    public int MMO_MAX_SEND_PER_PASS = 12;        // default 12
    public int MMO_MAX_READ_PER_PASS = 12;        // default 12
    public int MMO_HELPER_BUFFER_COUNT = 20;        // default 20
    public boolean ENABLE_MMOCORE_EXCEPTIONS = false;
    public boolean ENABLE_MMOCORE_DEBUG = false;
    public boolean ENABLE_MMOCORE_DEVELOP = false;
    /**
     * Client Packets Queue settings
     */
    public int CLIENT_PACKET_QUEUE_SIZE;    // default MMO_MAX_READ_PER_PASS + 2
    public int CLIENT_PACKET_QUEUE_MAX_BURST_SIZE;    // default MMO_MAX_READ_PER_PASS + 1
    public int CLIENT_PACKET_QUEUE_MAX_PACKETS_PER_SECOND;    // default 80
    public int CLIENT_PACKET_QUEUE_MEASURE_INTERVAL;    // default 5
    public int CLIENT_PACKET_QUEUE_MAX_AVERAGE_PACKETS_PER_SECOND;    // default 40
    public int CLIENT_PACKET_QUEUE_MAX_FLOODS_PER_MIN;    // default 2
    public int CLIENT_PACKET_QUEUE_MAX_OVERFLOWS_PER_MIN;    // default 1
    public int CLIENT_PACKET_QUEUE_MAX_UNDERFLOWS_PER_MIN;    // default 1
    public int CLIENT_PACKET_QUEUE_MAX_UNKNOWN_PER_MIN;    // default 5
    //Packets flooding Config
    public boolean DISABLE_FULL_PACKETS_FLOOD_PROTECTOR;
    public int FLOOD_PACKET_PROTECTION_INTERVAL;
    public boolean LOG_PACKET_FLOODING;
    public int PACKET_FLOODING_PUNISHMENT_LIMIT;
    public String PACKET_FLOODING_PUNISHMENT_TYPE;
    public String PROTECTED_OPCODES;
    public FastList<Integer> GS_LIST_PROTECTED_OPCODES = new FastList<>();
    public FastList<Integer> GS_LIST_PROTECTED_OPCODES2 = new FastList<>();
    public FastList<Integer> LS_LIST_PROTECTED_OPCODES = new FastList<>();
    public String ALLOWED_OFFLINE_OPCODES;
    public FastList<Integer> LIST_ALLOWED_OFFLINE_OPCODES = new FastList<>();
    public FastList<Integer> LIST_ALLOWED_OFFLINE_OPCODES2 = new FastList<>();
    public boolean DUMP_CLOSE_CONNECTIONS;

    private NetworkConfig() {
    }

    public static NetworkConfig getInstance() {
        if (networkConfig == null)
            networkConfig = new NetworkConfig();
        return networkConfig;
    }

    @Override
    public void configurationLoad() {
        ENABLE_MMOCORE_EXCEPTIONS = configManager.getBoolean("EnableMMOCoreExceptions");
        ENABLE_MMOCORE_DEBUG = configManager.getBoolean("EnableMMOCoreDebug");
        ENABLE_MMOCORE_DEVELOP = configManager.getBoolean("EnableMMOCoreDevelop");
        PACKET_HANDLER_DEBUG = configManager.getBoolean("PacketHandlerDebug");

        //flooding protection
        DISABLE_FULL_PACKETS_FLOOD_PROTECTOR = configManager.getBoolean("DisableOpCodesFloodProtector");
        FLOOD_PACKET_PROTECTION_INTERVAL = configManager.getInteger("FloodPacketProtectionInterval");
        LOG_PACKET_FLOODING = configManager.getBoolean("LogPacketFlooding");
        PACKET_FLOODING_PUNISHMENT_LIMIT = configManager.getInteger("PacketFloodingPunishmentLimit");
        PACKET_FLOODING_PUNISHMENT_TYPE = configManager.getString("PacketFloodingPunishmentType");

        //CLIENT-QUEUE-SETTINGS
        CLIENT_PACKET_QUEUE_SIZE = configManager.getInteger("ClientPacketQueueSize");
        CLIENT_PACKET_QUEUE_MAX_BURST_SIZE = configManager.getInteger("ClientPacketQueueMaxBurstSize");
        CLIENT_PACKET_QUEUE_MAX_PACKETS_PER_SECOND = configManager.getInteger("ClientPacketQueueMaxPacketsPerSecond");    // default 80
        CLIENT_PACKET_QUEUE_MEASURE_INTERVAL = configManager.getInteger("ClientPacketQueueMeasureInterval");    // default 5
        CLIENT_PACKET_QUEUE_MAX_AVERAGE_PACKETS_PER_SECOND = configManager.getInteger("ClientPacketQueueMaxAveragePacketsPerSecond");    // default 40
        CLIENT_PACKET_QUEUE_MAX_FLOODS_PER_MIN = configManager.getInteger("ClientPacketQueueMaxFloodPerMin");    // default 6
        CLIENT_PACKET_QUEUE_MAX_OVERFLOWS_PER_MIN = configManager.getInteger("ClientPacketQueueOverflowsPerMin");    // default 3
        CLIENT_PACKET_QUEUE_MAX_UNDERFLOWS_PER_MIN = configManager.getInteger("ClientPacketQueueUnderflowsPerMin");    // default 3
        CLIENT_PACKET_QUEUE_MAX_UNKNOWN_PER_MIN = configManager.getInteger("ClientPacketQueueUnknownPerMin");    // default 5

        PROTECTED_OPCODES = configManager.getString("ListOfProtectedOpCodes");

        LS_LIST_PROTECTED_OPCODES = new FastList<>();
        GS_LIST_PROTECTED_OPCODES = new FastList<>();
        GS_LIST_PROTECTED_OPCODES2 = new FastList<>();

        if (PROTECTED_OPCODES != null && !PROTECTED_OPCODES.equals("")) {

            StringTokenizer st = new StringTokenizer(PROTECTED_OPCODES, ";");

            while (st.hasMoreTokens()) {

                String token = st.nextToken();

                String[] token_splitted = null;

                if (token != null && !token.equals("")) {
                    token_splitted = token.split(",");
                } else {
                    continue;
                }

                if (token_splitted == null || token_splitted.length < 2) {
                    continue;
                }

                String server = token_splitted[0];

                if (server.equalsIgnoreCase("g")) {

                    String opcode1 = token_splitted[1].substring(2);
                    String opcode2 = "";

                    if (token_splitted.length == 3 && opcode1.equals("0xd0")) {
                        opcode2 = token_splitted[2].substring(2);
                    }

                    if (opcode1 != null && !opcode1.equals("")) {
                        GS_LIST_PROTECTED_OPCODES.add(Integer.parseInt(opcode1, 16));
                    }

                    if (opcode2 != null && !opcode2.equals("")) {
                        GS_LIST_PROTECTED_OPCODES2.add(Integer.parseInt(opcode2, 16));
                    }

                } else if (server.equalsIgnoreCase("l")) { //login opcode

                    LS_LIST_PROTECTED_OPCODES.add(Integer.parseInt(token_splitted[1].substring(2), 16));

                }

            }

        }

        ALLOWED_OFFLINE_OPCODES = configManager.getString("ListOfAllowedOfflineOpCodes");

        LIST_ALLOWED_OFFLINE_OPCODES = new FastList<>();
        LIST_ALLOWED_OFFLINE_OPCODES2 = new FastList<>();

        if (ALLOWED_OFFLINE_OPCODES != null && !ALLOWED_OFFLINE_OPCODES.equals("")) {

            StringTokenizer st = new StringTokenizer(ALLOWED_OFFLINE_OPCODES, ";");

            while (st.hasMoreTokens()) {

                String token = st.nextToken();

                String[] token_splitted = null;

                if (token != null && !token.equals("")) {
                    token_splitted = token.split(",");
                } else {
                    continue;
                }

                if (token_splitted == null || token_splitted.length == 0 || token_splitted[0].length() <= 3) {
                    continue;
                }

                String opcode1 = token_splitted[0].substring(2);

                if (opcode1 != null && !opcode1.equals("")) {
                    LIST_ALLOWED_OFFLINE_OPCODES.add(Integer.parseInt(opcode1, 16));
                    if (token_splitted.length > 1 && opcode1.equals("d0")) {
                        for (int i = 1; i < token_splitted.length; i++) {
                            if (token_splitted[i].length() <= 3) {
                                break;
                            }

                            String opcode2 = token_splitted[i].substring(2);
                            if (opcode2 != null && !opcode2.equals("")) {
                                LIST_ALLOWED_OFFLINE_OPCODES2.add(Integer.parseInt(opcode2, 16));
                            }
                        }
                    }
                }
            }

        }
        DUMP_CLOSE_CONNECTIONS = configManager.getBoolean("DumpCloseConnectionLogs");
    }
}