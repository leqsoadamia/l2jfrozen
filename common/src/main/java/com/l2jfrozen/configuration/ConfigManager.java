package com.l2jfrozen.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

/**
 * Server config manger
 * Contains all properties
 * <p/>
 * User: vdidenko
 * Date: 11/17/13
 * Time: 6:09 PM
 */
public class ConfigManager {
    //Config path
    private static final String CONFIG_DIRECTORY = "config";
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigManager.class);
    //config properties list
    private static Map<Object, Object> configList;
    private static ConfigManager configManager;
    private List<ConfigManagerObserver> observers = new ArrayList<>();

    /**
     * Constructor
     */
    public ConfigManager() {

    }

    /**
     * Global access method
     *
     * @return ConfigManager
     */
    public static ConfigManager getInstance() {
        if (configManager == null) {
            configManager = new ConfigManager();
        }
        return configManager;
    }

    private void loadProperties() {
        configList=new HashMap<>();
        parseConfig(CONFIG_DIRECTORY);
        LOGGER.info("Found {} properties", configList.size());
        this.onConfigLoad();
    }

    /**
     * @param path - property file path
     */
    private void parseConfig(String path) {
        File directory = new File(path);
        if (!directory.exists()) {
            LOGGER.error("config directory not found");
            return;
        }
        File[] files = directory.listFiles();
        assert files != null;
        if (files.length == 0) {
            LOGGER.error("Empty config folder");
        }
        for (File file : files) {
            if (file.isDirectory()) {
                parseConfig(file.getPath());
            } else if (file.getName().endsWith(".properties")) {
                parseConfigFile(file);
            }
        }
    }

    /**
     * @param file - configuration file
     */
    private void parseConfigFile(File file) {
        Properties optionsSettings = new Properties();
        InputStream is;
        try {
            is = new FileInputStream(file);
            optionsSettings.load(is);
            is.close();
            for (Map.Entry<Object, Object> entry : optionsSettings.entrySet()) {
                Object key = entry.getKey();
                if (configList.containsKey(key)) {
                    LOGGER.warn("-- Property: '{}' duplicate.", key);
                    continue;
                }
                configList.put(entry.getKey(), entry.getValue());
            }

        } catch (Exception e) {
            LOGGER.warn("Parse fault", e);
        }
    }

    /**
     * @param property - Property name
     * @return - property integer value
     */
    public int getInteger(String property) {
        Object prop = getProperty(property);
        if (prop == null) {
            return 0;
        }
        return Integer.decode(prop.toString());
    }

    /**
     * @param property - Property name
     * @return - property integer value
     */
    public long getLong(String property) {
        Object prop = getProperty(property);
        if (prop == null) {
            return 0;
        }
        return Long.parseLong(prop.toString());
    }

    /**
     * @param property - Property name
     * @return - property string value
     */
    public String getString(String property) {
        Object prop = getProperty(property);
        if (prop == null) {
            return "";
        }
        return prop.toString();
    }

    /**
     * @param property - Property name
     * @return - property byte value
     */
    public Byte getByte(String property) {
        Object prop = getProperty(property);
        if (prop == null) {
            return 0;
        }
        return Byte.parseByte(prop.toString());
    }

    /**
     * @param property - Property name
     * @return - property boolean value
     */
    public Boolean getBoolean(String property) {
        Object prop = getProperty(property);
        return prop != null && Boolean.parseBoolean(prop.toString());
    }

    /**
     * @param property - Property name
     * @return - property float value
     */
    public float getFloat(String property) {
        Object prop = getProperty(property);
        if (prop == null) {
            return 0;
        }
        return Float.valueOf(prop.toString());
    }

    /**
     * @param property - Property name
     * @return - property float value
     */
    public double getDouble(String property) {
        Object prop = getProperty(property);
        if (prop == null) {
            return 0;
        }
        return Double.parseDouble(prop.toString());
    }

    /**
     * Return array values for property key
     *
     * @param property     - property name
     * @param splitChar    - property delimiter
     * @param propertyType property type {@link PropertyType}
     * @return list of values
     */
    public ArrayList getArray(String property, String splitChar, PropertyType propertyType) {

        ArrayList list = new ArrayList();

        String prop = getString(property);
        String[] values = prop.split(splitChar);
        Object val = null;
        try{
            for (String s : values) {
                switch (propertyType) {
                    case INTEGER:
                        val = Integer.decode(s);
                        break;
                    case STRING:
                        val = s;
                        break;
                    case BOOLEAN:
                        val = Boolean.getBoolean(s);
                        break;
                }
                list.add(val);
            }
        }catch (Exception e){
            LOGGER.error("Parse properties \'{}\' error",prop);
        }

        return list;
    }

    /**
     * Return hash list of values by property name
     *
     * @param property -property name
     * @return hash list of values
     */
    public HashMap<Integer, Integer> getHashMap(String property) {
        HashMap<Integer, Integer> props = new HashMap<>();
        String prop = getString(property);
        if (prop.length() == 0) {
            LOGGER.warn("Empty property: {}", property);
            return props;
        }
        String[] v = prop.split(";");
        if (v.length == 0) {
            LOGGER.warn("Empty property: {}", property);
            return props;
        }
        for (String item : v) {
            Integer index = Integer.decode(item.split(",")[0]);
            Integer value = Integer.decode(item.split(",")[1]);
            props.put(index, value);
        }
        return props;
    }

    /**
     * Found property by name
     *
     * @param property - property name
     * @return - property value
     */
    private Object getProperty(String property) {
        Object prop;
        prop = configList.get(property);
        if (prop == null) {
            LOGGER.error("Property {} not found", property);
            return null;
        }
        return prop;
    }

    /**
     * Added config observer
     * @param observer - observer
     */
    public void addObserver(ConfigManagerObserver observer) {
        observers.add(observer);
    }

    /**
     * Remove observer
     * @param observer
     */
    public void removeObserver(ConfigManagerObserver observer) {
        observers.remove(observer);
    }


    /**
     * Called of config load done
     */
    private void onConfigLoad() {
        for (ConfigManagerObserver observer : observers) {
            observer.configurationLoad();
        }
    }

    /**
     * This method reload config
     */
    public void nativeReloadConfig() {
        loadProperties();
    }

    /**
     * Used to drive the data to array type
     */
    public enum PropertyType {
        /**
         * Integer type
         */
        INTEGER,
        /**
         * String type
         */
        STRING,
        /**
         * Boolean type
         */
        BOOLEAN,
        /**
         * Long type
         */
        LONG
    }

}
