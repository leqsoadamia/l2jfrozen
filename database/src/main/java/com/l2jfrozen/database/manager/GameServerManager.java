package com.l2jfrozen.database.manager;

import com.l2jfrozen.database.model.Account;
import com.l2jfrozen.database.model.GameServer;

import java.util.List;
import java.util.Map;

/**
 * User: vdidenko
 * Date: 26.11.13
 * Time: 23:49
 */
public interface GameServerManager {

    /**
     * Get all registered game server
     *
     * @return list of {@link GameServer}
     */
    public List<GameServer> getAll();

    /**
     * Check is game server is register
     *
     * @param id server id
     * @return true if server is register
     */
    public boolean isRegisterServer(int id);

    public void clear();

    /**
     * Add new game server to db and cache list
     *
     * @param id   server id
     * @param host host name
     * @return new game server if new server added successfully
     */
    public GameServer add(int id, String host, String hex);

    /**
     * Return all accounts for server
     *
     * @param server game server
     * @return List of Account
     */
    List<Account> getAccounts(GameServer server);

    /**
     * Add new account to server
     * @param server game server
     * @param account user account
     * @return current game server
     */
    void addAccount(GameServer server, Account account);

    void removeAccount(GameServer server, Account account);

    public Map<Integer, String> getServerNames();

    public String getServerName(int serverId);

    public GameServer getServer(int serverId);
}
