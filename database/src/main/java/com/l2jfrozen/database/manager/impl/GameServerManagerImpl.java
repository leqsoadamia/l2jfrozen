package com.l2jfrozen.database.manager.impl;

import com.l2jfrozen.database.dao.manager.impl.AccountManagerDAOImpl;
import com.l2jfrozen.database.dao.manager.impl.GameServerManagerDAOImpl;
import com.l2jfrozen.database.manager.GameServerManager;
import com.l2jfrozen.database.model.Account;
import com.l2jfrozen.database.model.GameServer;
import javolution.io.UTF8StreamReader;
import javolution.util.FastMap;
import javolution.xml.stream.XMLStreamConstants;
import javolution.xml.stream.XMLStreamException;
import javolution.xml.stream.XMLStreamReaderImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * User: vdidenko
 * Date: 26.11.13
 * Time: 23:49
 */
public class GameServerManagerImpl implements GameServerManager {
    protected static final Logger LOGGER = LoggerFactory.getLogger(GameServerManagerImpl.class);
    private static GameServerManager gameServerManager;
    // Server Names Config
    private Map<Integer, String> serverNames = new FastMap<>();

    private GameServerManagerImpl() {
        loadServerNames();
    }

    public static GameServerManager getInstance() {
        if (gameServerManager == null) {
            gameServerManager = new GameServerManagerImpl();
        }
        return gameServerManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<GameServer> getAll() {
        return GameServerManagerDAOImpl.getInstance().getAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRegisterServer(int id) {
        for (GameServer gameServer : getAll()) {
            if (gameServer.getId() == id) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void clear() {
        GameServerManagerDAOImpl.getInstance().clear();
    }

    @Override
    public GameServer add(int id, String host, String hex) {
        GameServer gameServer = new GameServer();
        gameServer.setId(id);
        gameServer.setHexid(hex);
        gameServer.setHost(host);
        gameServer = GameServerManagerDAOImpl.getInstance().add(gameServer);
        if (gameServer == null) {
            return null;
        }
        return gameServer;
    }

    @Override
    public List<Account> getAccounts(GameServer server) {
        return GameServerManagerDAOImpl.getInstance().get(server.getId()).getAccounts();
    }

    @Override
    public void addAccount(GameServer server, Account account) {
        account.setLastServer(server.getId());
        try {
            AccountManagerDAOImpl.getInstance().add(account);
            server.getAccounts().add(account);
        } catch (SQLException e) {
            LOGGER.error("Account not save", e);
        }
    }

    @Override
    public void removeAccount(GameServer server, Account account) {
        for (Account acc : server.getAccounts()) {
            if (acc.getLogin().equals(account.getLogin())) {
                server.getAccounts().remove(account);
            }
        }
    }

    void loadServerNames() {
        XMLStreamReaderImpl xpp = new XMLStreamReaderImpl();
        UTF8StreamReader reader = new UTF8StreamReader();

        InputStream in = null;
        try {
            File conf_file = new File("./config/servername.xml");

            in = new FileInputStream(conf_file);
            xpp.setInput(reader.setInput(in));

            for (int e = xpp.getEventType(); e != XMLStreamConstants.END_DOCUMENT; e = xpp.next()) {
                if (e == XMLStreamConstants.START_ELEMENT) {
                    if (xpp.getLocalName().toString().equals("server")) {
                        Integer id = new Integer(xpp.getAttributeValue(null, "id").toString());
                        String name = xpp.getAttributeValue(null, "name").toString();
                        serverNames.put(id, name);
                    }
                }
            }
            LOGGER.info("Loaded " + serverNames.size() + " server names");

        } catch (FileNotFoundException e) {

            LOGGER.warn("servername.xml could not be loaded: file not found");
        } catch (XMLStreamException xppe) {
            LOGGER.error("", xppe);
        } finally {
            try {
                xpp.close();
            } catch (XMLStreamException e) {
                LOGGER.error("", e);
            }

            try {
                reader.close();
            } catch (IOException e) {
                LOGGER.error("", e);
            }

            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    LOGGER.error("", e);
                }
            }
        }
    }

    public Map<Integer, String> getServerNames() {
        return serverNames;
    }

    @Override
    public String getServerName(int serverId) {
        return serverNames.get(serverId);
    }

    @Override
    public GameServer getServer(int serverId) {
        return GameServerManagerDAOImpl.getInstance().get(serverId);
    }
}
