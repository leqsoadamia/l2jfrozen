package com.l2jfrozen.database.connection;

import com.l2jfrozen.configuration.DatabaseConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: vdidenko
 * <p/>
 * <p/>
 * Date: 26.11.13
 * Time: 23:11
 */
public abstract class DatabaseManager {
    protected static final Logger LOGGER = LoggerFactory.getLogger(DatabaseManager.class);
    private DatabaseController databaseController;

    protected DatabaseManager() {
        final DatabaseConfig databaseConfig=new DatabaseConfig();
        databaseConfig.configurationLoad();
        databaseController = DatabaseController.getInstance();
        databaseController.setConfig(databaseConfig);

        try {
            databaseController.initialize();
        } catch (SQLException e) {
            LOGGER.error("Connection initialize fault", e);
        }
    }

    /**
     * Prepare query and execute
     *
     * @param query for simple {@code 'SELECT * FROM test_table'}
     * @return query result
     */
    public PreparedStatement executeQuery(String query) {
        Connection connection;
        PreparedStatement statement=null;
        try {
            connection = databaseController.getConnection();
            statement = connection.prepareStatement(query);
            close();
        } catch (SQLException e) {
            LOGGER.error("Connection open fault", e);
        }
        return statement;
    }

    /**
     * Open connection and create statement but not execute query
     *
     * @param query for simple {@code 'SELECT * FROM test_table'}
     * @return statement
     */
    public PreparedStatement prepareQuery(String query) {
        PreparedStatement statement = null;
        try {
            Connection connection = databaseController.getConnection();
            statement = connection.prepareStatement(query);

        } catch (SQLException e) {
            LOGGER.error("Connection open fault", e);
        }
        return statement;
    }
    public void close(){
        databaseController.close();
    }
    public void close(Connection connection){
        databaseController.close(connection);
    }
    public void closeStatement(PreparedStatement preparedStatement){
        try{
           if(preparedStatement!=null){
               preparedStatement.close();
           }
        } catch (SQLException e) {
            LOGGER.error("Fault close statement");
        }
    }
    public void closeResultSet(ResultSet resultSet){
        try{
            if(resultSet!=null){
                resultSet.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Fault close resultSet");
        }
    }
}
