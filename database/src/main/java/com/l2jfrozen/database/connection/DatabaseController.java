package com.l2jfrozen.database.connection;

import com.l2jfrozen.configuration.DatabaseConfig;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Database connection controller
 * IMPORTANT!!! This should use only this package
 * All operation on database - implement other managers
 */
class DatabaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseController.class);
    private DatabaseConfig databaseConfig;
    private final Map<String, Connection> connectionMap = new HashMap<>();
    private ComboPooledDataSource comboPooledDataSource;
    private static DatabaseController databaseController;
    private boolean initialized = false;

    private DatabaseController() {
    }

    public static DatabaseController getInstance() {
        if (databaseController == null) {
            databaseController = new DatabaseController();
        }
        return databaseController;
    }

    /**
     * Close all connection to database and clear connection list
     */
    public void closeAllConnection() {
        for (Map.Entry<String, Connection> entry : connectionMap.entrySet()) {
            try {
                entry.getValue().close();
            } catch (SQLException e) {
                LOGGER.error("Close connection error", e);
            }
        }
        connectionMap.clear();
    }

    /**
     * Close connection to database
     *
     * @param connection connection
     * @return true if connection found and close
     */
    public boolean close(Connection connection) {
        Iterator iterator = connectionMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry item = (Map.Entry) iterator.next();
            if (item.getValue() == connection) {
                try {
                    connection.close();
                    iterator.remove();
                    return true;
                } catch (SQLException e) {
                    LOGGER.error("Close connection error", e);
                }
            }
        }
        return false;
    }

    public boolean close() {
        String key = generateKey();
        if (connectionMap.containsKey(key)) {

            connectionMap.remove(key);
        }
        return false;
    }

    /**
     * Initialize connector
     *
     * @throws SQLException
     */
    public void initialize() throws SQLException {
        if (initialized) {
            return;
        }
        try {
            if (databaseConfig.getMaxConnections() < 2) {
                databaseConfig.setMaxConnections(2);
                LOGGER.warn("A minimum of " + databaseConfig.getMaxConnections() + " db connections are required.");
            }

            comboPooledDataSource = new ComboPooledDataSource();
            comboPooledDataSource.setAutoCommitOnClose(true);

            comboPooledDataSource.setInitialPoolSize(10);
            comboPooledDataSource.setMinPoolSize(10);
            comboPooledDataSource.setMaxPoolSize(Math.max(10, databaseConfig.getMaxConnections()));

            comboPooledDataSource.setAcquireRetryAttempts(0); // try to obtain connections indefinitely (0 = never quit)
            comboPooledDataSource.setAcquireRetryDelay(500); // 500 milliseconds wait before try to acquire connection again
            comboPooledDataSource.setCheckoutTimeout(0); // 0 = wait indefinitely for new connection
            // if pool is exhausted
            comboPooledDataSource.setAcquireIncrement(5); // if pool is exhausted, get 5 more connections at a time

            comboPooledDataSource.setAutomaticTestTable("connection_test_table");
            comboPooledDataSource.setTestConnectionOnCheckin(false);


            comboPooledDataSource.setIdleConnectionTestPeriod(3600); // test idle connection every 60 sec
            comboPooledDataSource.setMaxIdleTime(databaseConfig.getMaxIdleTime()); // 0 = idle connections never expire

            comboPooledDataSource.setMaxStatementsPerConnection(100);

            comboPooledDataSource.setBreakAfterAcquireFailure(false); // never fail if any way possible

            comboPooledDataSource.setDriverClass(databaseConfig.getDriver());
            comboPooledDataSource.setJdbcUrl(databaseConfig.getUrl());
            comboPooledDataSource.setUser(databaseConfig.getLogin());
            comboPooledDataSource.setPassword(databaseConfig.getPassword());

			/* Test the connection */
            comboPooledDataSource.getConnection().close();

            if (databaseConfig.isDebugMode())
                LOGGER.debug("Database Connection Working");
        } catch (SQLException x) {
            if (databaseConfig.isDebugMode())
                LOGGER.error("Database Connection FAILED");
            // re-throw the exception
            throw x;
        } catch (Exception e) {
            if (databaseConfig.isDebugMode())
                LOGGER.error("Database Connection FAILED");
            throw new SQLException("Could not init DB connection:" + e.getMessage());
        }
        initialized = true;
    }

    public Connection getConnection() throws SQLException {
        Connection connection;

        String key = generateKey();

        connection = connectionMap.get(key);
        if (connection == null) {
            try {
                connection = comboPooledDataSource.getConnection();
            } catch (SQLException e) {
                LOGGER.warn("Couldn't create connection. Cause: " + e.getMessage());
            }
        }

        if (connection != null) {
            synchronized (connectionMap) {
                connectionMap.put(key, connection);
            }
        }
        return connection;
    }

    /**
     * Generate connection key
     * <p/>
     * Key equal thread hash code
     *
     * @return key.
     */
    public String generateKey() {
        return String.valueOf(Thread.currentThread().hashCode());
    }

    public void shutdown() {
        connectionMap.clear();

        try {
            comboPooledDataSource.close();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("", e);
        }
        try {
            comboPooledDataSource = null;
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("", e);
        }
    }

    public void setConfig(DatabaseConfig databaseConfig) {
        this.databaseConfig = databaseConfig;
    }

    public int getConnectionCount() {
        return connectionMap.size();
    }
}
