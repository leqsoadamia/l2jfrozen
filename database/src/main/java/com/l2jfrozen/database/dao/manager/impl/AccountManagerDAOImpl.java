package com.l2jfrozen.database.dao.manager.impl;

import com.l2jfrozen.database.connection.DatabaseManager;
import com.l2jfrozen.database.dao.manager.AccountManagerDAO;
import com.l2jfrozen.database.model.Account;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * vdidenko
 * 05.12.13
 */
public class AccountManagerDAOImpl extends DatabaseManager implements AccountManagerDAO {

    private static AccountManagerDAO accountManagerDAO;

    public static AccountManagerDAO getInstance() {
        if (accountManagerDAO == null) {
            accountManagerDAO = new AccountManagerDAOImpl();
        }
        return accountManagerDAO;
    }

    @Override
    public Account get(String login) throws SQLException {
        PreparedStatement statement = null;
        ResultSet executeQuery = null;
        try {
            statement = prepareQuery("SELECT * FROM accounts WHERE login=?");
            statement.setString(1, login);
            executeQuery = statement.executeQuery();
            close();

            if (executeQuery.next()) {
                return mappedAccount(executeQuery);
            }
        } catch (Exception e) {
            LOGGER.error("", e);
        } finally {
            closeStatement(statement);
            closeResultSet(executeQuery);
        }
        return null;
    }

    @Override
    public Account get(String login, String password) throws SQLException {
        PreparedStatement statement = null;
        ResultSet executeQuery = null;
        try {
            statement = prepareQuery("SELECT * FROM accounts WHERE login=? AND password=?");
            statement.setString(1, login);
            statement.setString(2, password);
            executeQuery = statement.executeQuery();

            if (executeQuery.next()) {
                return mappedAccount(executeQuery);
            }
        } catch (Exception e) {
            LOGGER.error("", e);
        } finally {
            closeStatement(statement);
            closeResultSet(executeQuery);
            close();
        }
        return null;
    }

    @Override
    public Account add(Account account) {
        account.setLastActive(System.currentTimeMillis());
        PreparedStatement statement = null;
        try {
            statement = prepareQuery("INSERT INTO accounts (login,password,last_active,access_level,last_server,lastIP) VALUES(?,?,?,?,?,?)");
            statement.setString(1, account.getLogin());
            statement.setString(2, account.getPassword());
            statement.setFloat(3, account.getLastActive());
            statement.setInt(4, account.getAccessLevel());
            statement.setInt(5, account.getLastServer());
            statement.setString(6, account.getLastIp());
            statement.executeUpdate();
            close();
            return account;
        } catch (SQLException e) {
            LOGGER.error("Account didn't save", e);
            return null;
        } finally {
            closeStatement(statement);
        }
    }

    @Override
    public Account update(Account account) {
        account.setLastActive(System.currentTimeMillis());
        PreparedStatement statement = null;
        try {
            statement = prepareQuery("UPDATE accounts SET login=?,password=?,access_level=?,last_active=?,last_server=?,lastIP=? WHERE login = ?");
            statement.setString(1, account.getLogin());
            statement.setString(2, account.getPassword());
            statement.setInt(3, account.getAccessLevel());
            statement.setFloat(4, account.getLastActive());
            statement.setInt(5, account.getLastServer());
            statement.setString(6, account.getLastIp());
            statement.setString(7, account.getLogin());
            statement.executeUpdate();
            close();
        } catch (SQLException e) {
            LOGGER.error("Change last server fault", e);
        } finally {
            closeStatement(statement);
        }
        return account;
    }

    private Account mappedAccount(ResultSet foundResult) throws SQLException {
        Account account = new Account();
        account.setLogin(foundResult.getString("login"));
        account.setPassword(foundResult.getString("password"));
        account.setLastActive(foundResult.getFloat("last_active"));
        account.setAccessLevel(foundResult.getInt("access_level"));
        account.setLastIp(foundResult.getString("lastIP"));
        account.setLastServer(foundResult.getInt("last_server"));
        return account;
    }
}
