package com.l2jfrozen.database.dao.manager;

import com.l2jfrozen.database.model.Account;

import java.sql.SQLException;

/**
 * vdidenko
 * 05.12.13
 */
public interface AccountManagerDAO {

    Account get(String login) throws SQLException;
    Account get(String login,String password) throws SQLException;
    Account add(Account account) throws SQLException;

    Account update(Account account);
}
