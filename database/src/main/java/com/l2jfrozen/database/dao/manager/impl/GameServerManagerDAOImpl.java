package com.l2jfrozen.database.dao.manager.impl;

import com.l2jfrozen.database.connection.DatabaseManager;
import com.l2jfrozen.database.dao.manager.GameServerManagerDAO;
import com.l2jfrozen.database.model.GameServer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: vdidenko
 * Date: 26.11.13
 * Time: 23:02
 */
public class GameServerManagerDAOImpl extends DatabaseManager implements GameServerManagerDAO {
    public static GameServerManagerDAO gameServerManager;
    private List<GameServer> serverList = new ArrayList<>();

    private GameServerManagerDAOImpl() {
        super();
        updateServerList();
    }

    public static GameServerManagerDAO getInstance() {
        if (gameServerManager == null) {
            gameServerManager = new GameServerManagerDAOImpl();
        }
        return gameServerManager;
    }

    public void updateServerList() {
        List<GameServer> serverList = new ArrayList<>();
        PreparedStatement preparedStatement=null;
        ResultSet resultSet=null;
        try {
           preparedStatement = executeQuery("SELECT * FROM gameservers");
           resultSet=preparedStatement.executeQuery();
            while (resultSet.next()) {
                GameServer gameServer = new GameServer();
                gameServer.setId(resultSet.getInt("server_Id"));
                gameServer.setHexid(resultSet.getString("hexid"));
                gameServer.setHost(resultSet.getString("host"));
                serverList.add(gameServer);

            }
            close();
            this.serverList.clear();
            this.serverList.addAll(serverList);
            LOGGER.info("Update server list, load {} servers", serverList.size());
        } catch (SQLException e) {
            LOGGER.error("Result iteration fault",e);
        }finally {
            closeResultSet(resultSet);
            closeStatement(preparedStatement);
        }
    }

    @Override
    public List<GameServer> getAll() {
        if (serverList.size() == 0) {
            updateServerList();
        }
        return serverList;
    }

    @Override
    public GameServer get(Integer id) {
        for(GameServer gameServer:getAll()){
            if(gameServer.getId().equals(id)){
                return gameServer;
            }
        }
        return null;
    }

    @Override
    public GameServer add(GameServer gameServer) {
        PreparedStatement statement = null;
        try {
            statement = prepareQuery("INSERT INTO gameservers (server_id,hexid,host) values (?,?,?)");
            statement.setInt(1, gameServer.getId());
            statement.setString(2, gameServer.getHexid());
            statement.setString(3, gameServer.getHost());
            statement.executeUpdate();
            serverList.add(gameServer);
            close();
        } catch (SQLException e) {
            LOGGER.error("Save game serve fault", e);
            return null;
        }finally {
            closeStatement(statement);
        }

        return gameServer;
    }

    @Override
    public void clear() {
        try{
            executeQuery("DELETE FROM gameservers");
        }catch (Exception e){
           LOGGER.error("",e);
        }
        executeQuery("DELETE FROM gameservers");
        serverList.clear();
    }
}
